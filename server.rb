#!/usr/bin/env ruby
# frozen_string_literal: true

require 'sinatra'
require 'sinatra/cors'
require 'fileutils'
require 'yaml'
require 'filemagic'
require 'json-schema'

require 'byebug' if ENV['DEV'] == '1'

CONFIG_SCHEMA = {
  type: :object,
  required: %i[input_dir output_dir keyboard],
  properties: {
    input_dir: { type: :string },
    output_dir: { type: :string },
    keyboard: {
      type: :object,
      required: %i[controls categories],
      properties: {
        controls: { type: :object },
        categories: { type: :object }
      }
    }
  }
}.freeze

def check_directories(input_dir, output_dir)
  return false unless File.directory? input_dir

  return false if !File.directory?(output_dir) && !FileUtils.mkdir_p(output_dir)

  true
end

# Configuration
presets_file = File.join(Dir.home, '.config', 'fsg.yml')
raise 'Configuration file not found. Check config.default.yml for reference' unless File.exist? presets_file

presets = YAML.load_file presets_file, aliases: true
presets.reject! { |k| k == '_references' }
# Set some defaults
presets.each_key do |preset|
  presets[preset]['ignores'] ||= []
end

raise 'No preset found in config' unless presets.keys.count.positive?

presets.each_value do |preset|
  JSON::Validator.validate!(CONFIG_SCHEMA, preset)
end

set :presets, presets

# CORS configuration: allow everything as we're on local
# Note that these settings are only useful when using frontend in dev mode
# (different port)
puts '>>> Set DEV to 1 if you need CORS to be enabled for everything'
puts '>>> Allowing CORS from everywhere' if ENV['DEV']
set :allow_origin, ENV['DEV'] && ENV['DEV'] == '1' ? '*' : ['http://localhost:4567', 'http://127.0.0.1:4567']
set :allow_methods, 'GET,HEAD,POST,PATCH'
set :allow_headers, 'content-type,if-modified-since'
set :expose_headers, 'location,link'

get '/' do
  send_file 'public/index.html'
end

get '/presets' do
  content_type 'application/json'

  presets.to_json
end

get '/files' do
  content_type 'application/json'
  list = []
  path = if params['recursive'] == 'true'
           "#{params['input_dir']}**/*"
         else
           "#{params['input_dir']}*"
         end

  Dir.glob(path).sort.each do |item|
    list.push(item.delete_prefix(params['input_dir'])) unless File.directory? item
  end

  list.to_json
end

get '/file/:file' do
  file = File.join(params['input_dir'], params[:file])
  content_type FileMagic.new(FileMagic::MAGIC_MIME).file file

  File.read(file)
end

post '/process' do
  content_type 'application/json'
  data    = request.body.read
  payload = JSON.parse(data)

  payload['files'].each_pair do |category, files|
    # Handle relative and absolute paths
    dest = if category.match?(%r{^/})
             category
           else
             File.join(payload['output_dir'], category)
           end

    FileUtils.mkdir_p(dest) unless File.directory? dest

    files.each do |file|
      filename    = File.basename(file)
      dest_file   = File.join(dest, filename)
      source_file = File.join(payload['input_dir'], file)
      if File.exist?(source_file) && File.exist?(dest_file) == false
        FileUtils.mv source_file, dest_file
      else
        puts "[WARNING] File #{file} (#{source_file}) does not exists anymore or is already present in #{dest_file}"
      end
    end
  end

  {}.to_json
end

patch '/check_paths' do
  content_type 'application/json'
  paths = JSON.parse(request.body.read)
  state = check_directories paths['input_dir'], paths['output_dir']

  error 422 unless state

  { ok: true }.to_json
end

post '/exit' do
  Sinatra::Application.quit!
end
