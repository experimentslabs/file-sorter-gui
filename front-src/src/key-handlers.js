import { Toast } from "toaster-js";
import c from "./config";

export default {
  next(plus = 1) {
    const next = this.$store.getters.currentFileIndex + plus;
    if (next >= 0 && next < this.files.length) {
      this.$store.dispatch("changeIndex", plus);
    } else {
      /* eslint-disable-next-line no-new */
      new Toast(
        "You reach the extremity of the list",
        Toast.TYPE_WARNING,
        c.toastDuration,
      );
    }
  },
  _reset(notify = true) {
    this.$store.dispatch("removeFromCategory", this.currentFile);

    if (notify) {
      /* eslint-disable-next-line no-new */
      new Toast("Removed from category", Toast.TYPE_INFO, c.toastDuration);
    }
  },
  _next() {
    this.next(1);
  },
  _previous() {
    this.next(-1);
  },
  _apply() {
    this.sendCurrent();
  },
  _category(category) {
    this._reset(false);
    this.categorizedIndexes.push(this.currentFileIndex);
    this.$store.dispatch("addInCategory", {
      category,
      file: this.currentFile,
    });

    /* eslint-disable-next-line no-new */
    new Toast(`Added in ${category}`, Toast.TYPE_MESSAGE, c.toastDuration);
    this.next(1);
  },
  _jump_to_first() {
    this.next(-this.currentFileIndex);
  },
  _jump_to_undone() {
    const elementsKeys = Object.keys(this.files);
    for (let i in elementsKeys) {
      i = parseInt(i, 10); // weird, but index is a string...
      if (this.categorizedIndexes.indexOf(i) === -1) {
        this.$store.dispatch("setIndex", i);
        return;
      }
    }

    /* eslint-disable-next-line no-new */
    new Toast(
      `No file left to review. You may apply the changes.`,
      Toast.TYPE_ERROR,
      c.toastDuration,
    );
  },
};
