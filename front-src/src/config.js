export default {
  spinnerOptions: {
    lines: 5, // The number of lines to draw
    length: 0, // The length of each line
    width: 4, // The line thickness
    radius: 30, // The radius of the inner circle
    scale: 1, // Scales overall size of the spinner
    corners: 0.7, // Corner roundness (0..1)
    color: "#ffffff", // CSS color or array of colors
    fadeColor: "#00000", // CSS color or array of colors
    speed: 1, // Rounds per second
    rotate: 54, // The rotation offset
    animation: "spinner-line-fade-quick", // The CSS animation name for the lines
    direction: 1, // 1: clockwise, -1: counterclockwise
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    className: "spinner", // The CSS class to assign to the spinner
    top: "50%", // Top position relative to parent
    left: "50%", // Left position relative to parent
    shadow: "0 0 10px black", // Box-shadow for the lines
    position: "absolute", // Element positioning
  },

  // Api URL
  apiUrl: "http://localhost:4567",

  // Correspondence between Sinatra configuration and event keys
  customKeyReferences: {
    right: "ArrowRight",
    left: "ArrowLeft",
    down: "ArrowDown",
    up: "ArrowUp",
    space: " ",
  },

  maxFileStack: 5,

  toastDuration: 1000,
};
