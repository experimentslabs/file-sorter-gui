import { createStore } from "vuex";

import configModule from "./modules/config";
import elementsModule from "./modules/elements";
import presetsModule from "./modules/presets";

export default createStore({
  state: {
    paused: false,
    zooming: false,
  },
  mutations: {
    setPause(state, newState) {
      state.paused = newState;
    },
    setZooming(state, newState) {
      state.zooming = newState;
    },
  },
  actions: {
    setPause: ({ commit }, newState) => {
      commit("setPause", newState);
    },
    setZooming: ({ commit }, newState) => {
      commit("setZooming", newState);
    },
  },
  getters: {
    paused: (state) => state.paused,
    zooming: (state) => state.zooming,
  },
  modules: {
    configModule,
    elementsModule,
    presetsModule,
  },
});
