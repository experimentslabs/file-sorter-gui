// FIXME: Refactor this
import api from "redaxios";
import c from "../../config";

export default {
  state: {
    config: {
      input_dir: null,
      output_dir: null,
      recursive: false,
      ignores: [],
      keyboard: {
        categories: {},
        controls: {},
      },
    },
    configName: null,
    serverExited: false,
  },
  mutations: {
    setConfig(state, config) {
      state.config = config;
    },
    setConfigName(state, name) {
      state.configName = name;
    },
    setConfigPaths(state, { inputDir, outputDir }) {
      state.config.input_dir = inputDir;
      state.config.output_dir = outputDir;
    },
    setServerExited(state, value) {
      state.serverExited = value;
    },
    setConfigValue(state, { key, value }) {
      state.config[key] = value;
    },
  },
  actions: {
    exit({ commit }) {
      return (
        api
          .post(`${c.apiUrl}/exit`)
          // Server should be closed; if response is successful there is a problem.
          .then((error) => {
            console.warn("Error in exit", error);
            return Promise.reject(error);
          })
          .catch(() => {
            commit("setServerExited", true);
            return Promise.resolve;
          })
      );
    },
    setConfig({ commit, dispatch }, { name, config }) {
      const paths = {
        input_dir: config.input_dir,
        output_dir: config.output_dir,
      };
      return dispatch("checkConfigPaths", paths)
        .then(() => {
          commit("setConfig", config);
          commit("setConfigName", name);
          return dispatch("loadFilePaths");
        })
        .catch((error) => {
          console.warn("Error in setConfig", error);
          return Promise.reject(error);
        });
    },
    toggleRecursive({ state, commit, dispatch }, value) {
      if (state.config.recursive === value) return;

      commit("setConfigValue", { key: "recursive", value });
      dispatch("loadFilePaths");
    },
    checkConfigPaths: ({ commit, dispatch, state }, paths) =>
      api.patch(`${c.apiUrl}/check_paths`, paths).catch((error) => {
        console.warn("Error in updateConfigPaths", error);
        return Promise.reject(error);
      }),
    updateConfigPaths: ({ commit, dispatch, state }, paths) =>
      dispatch("checkConfigPaths", paths)
        .then(() => {
          const currentInputPath = state.config.input_dir;
          commit("setConfigPaths", {
            inputDir: paths.input_dir,
            outputDir: paths.output_dir,
          });
          // Only reload if input path changed
          if (currentInputPath !== paths.input_dir) dispatch("loadFilePaths");
        })
        .catch((error) => {
          console.warn("Error in updateConfigPaths", error);
          return Promise.reject(error);
        }),
  },
  getters: {
    serverExited: (state) => state.serverExited,
    config: (state) => state.config,
    configName: (state) => state.configName,
    categories: (state) =>
      Object.values(state.config.keyboard.categories).filter(
        (category, index, array) => array.indexOf(category) === index,
      ),
    ignores: (state) => state.config.ignores,
    ignoresExpressions: (state) =>
      state.config.ignores.map(/** @param {string} i */ (i) => new RegExp(i)),
    categoriesWithKeys: (state) => {
      const out = {};
      for (const type of Object.keys(state.config.keyboard)) {
        out[type] = {};

        for (const key of Object.keys(state.config.keyboard[type])) {
          const element = state.config.keyboard[type][key];
          if (!out[type][element]) {
            out[type][element] = [];
          }
          out[type][element].push(key);
        }
      }
      return out;
    },
  },
};
