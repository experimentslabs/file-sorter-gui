import api from "redaxios";
import c from "../../config";

export default {
  state: {
    presets: {},
  },
  mutations: {
    setPresets(state, presets) {
      state.presets = presets;
    },
  },
  actions: {
    loadPresets: ({ dispatch, commit, getters }) =>
      api
        .get(`${c.apiUrl}/presets`)
        .then(({ data }) => {
          commit("setPresets", data);
          const keys = Object.keys(data);
          if (!getters.configName && keys.length > 0) {
            dispatch("setConfig", { name: keys[0], config: data[keys[0]] });
          }
        })
        .catch((error) => {
          console.warn("Error in loadPresets", error);
          return Promise.reject(error);
        }),
  },
  getters: {
    presets: (state) => state.presets,
  },
};
