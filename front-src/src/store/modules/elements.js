// FIXME: Refactor this
import api from "redaxios";
import c from "../../config";

export default {
  state: {
    elements: [],
    currentFileIndex: null,
    categorized: {},
  },
  mutations: {
    initializeElements(state, { elements, config }) {
      state.elements = elements;
      state.currentFileIndex = 0;
      const categories = {};
      Object.keys(config.keyboard.categories).forEach((e) => {
        categories[config.keyboard.categories[e]] = [];
      });

      state.categorized = categories;
    },
    changeIndex(state, amount) {
      state.currentFileIndex += amount;
    },
    setIndex(state, index) {
      state.currentFileIndex = index;
    },
    removeFromCategory(state, file) {
      const categories = Object.keys(state.categorized);
      for (const category of categories) {
        const index = state.categorized[category].indexOf(file);
        if (index > -1) {
          state.categorized[category].splice(index, 1);
          return true;
        }
      }
      return false;
    },
    addInCategory(state, { category, file }) {
      state.categorized[category].push(file);
    },
  },
  actions: {
    loadFilePaths: ({ commit, getters }) => {
      const config = getters.config;
      return api
        .get(`${c.apiUrl}/files`, {
          params: { input_dir: config.input_dir, recursive: config.recursive },
        })
        .then(({ data }) => {
          const ignores = getters.ignoresExpressions;
          const elements = data.filter((e) => {
            for (const i of ignores) {
              if (e.match(i)) return false;
            }
            return true;
          });
          commit("initializeElements", { elements, config });
        })
        .catch((error) => {
          console.warn("Error in loadFilePaths", error);
          return Promise.reject(error);
        });
    },
    saveFiles: ({ dispatch }, payload) =>
      api
        .post(`${c.apiUrl}/process`, payload)
        .then(() => dispatch("loadFilePaths"))
        .catch((error) => {
          console.warn("Error in saveFiles", error);
          return Promise.reject(error);
        }),
    changeIndex: ({ commit }, amount) => {
      commit("changeIndex", amount);
    },
    setIndex: ({ commit }, index) => {
      commit("setIndex", index);
    },
    removeFromCategory: ({ commit }, file) => {
      commit("removeFromCategory", file);
    },
    addInCategory: ({ commit }, data) => {
      commit("addInCategory", data);
    },
  },
  getters: {
    files: (state) => state.elements,
    currentFileIndex: (state) => state.currentFileIndex,
    currentFile: (state, getters) => state.elements[getters.currentFileIndex],
    totalFiles: (state) => state.elements.length,
    categorized: (state) => state.categorized,
    categorizedFilesAmount: (state) => {
      const categories = Object.keys(state.categorized);
      let amount = 0;
      for (const category of categories) {
        amount += state.categorized[category].length;
      }
      return amount;
    },
    isCategorized: (state) => (file) => {
      const categories = Object.keys(state.categorized);
      for (const name of categories) {
        if (state.categorized[name].indexOf(file) > -1) return name;
      }
      return false;
    },
  },
};
