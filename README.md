# File sorter GUI

> Visually helps in sorting folders with big amounts of files.

Once configured, the GUI will allow you to cycle through the files of a
given folder and move them in categories with a keystroke.

For picture files (png/gif/jpg), the file is displayed. For other files,
only the filename is displayed.

## Configuration

Copy `config.default.yml` to `~/.config/fsg.yml` and edit it (the file should be
commented enough for you to understand)

## Start

This has been tested on ruby 2.3.7 and 2.5.1

"libmagic-dev" is needed to compile "ruby-filemagic"

Install the dependencies:

```sh
bundle install

# Launch server
ruby server.rb

# Or
ruby server.rb <INPUT_DIR> <OUTPUT_DIR>
```

Open Firefox and head to http://localhost:4567

## Develop

### Backend
The backend is a sinatra application; edit `server.rb` and you're done.

### Frontend
To allow CORS requests, use `DEV=1 ./server.rb` during front development

The frontend is a VueJS application, located in `front-src`. To start
hacking it, cd in the `front-src` directory and

```sh
yarn install

# Start project on port 5173
yarn dev

# Lint:
yarn lint

# Build in "public" directory:
yarn build
```

Build is only needed for releases.

While you develop, start the server normally, but open your browser on
http://localhost:5173/ to display the version you're working on.
If you use http://localhost:4567, it will be the built one.

## Background

One day I generated a lot of pictures (5000+) with
the [Neural Artistic Style](https://github.com/andersbll/neural_artistic_style)
project. All pictures needed to be manually reviewed to find which sets of 
presets were the best.

Given the amount of files to sort, I created this tool to speed up the process.

## Notes

Well, even if this tool creates a server, it's not meant to be exposed...
